import React from "react"
import Title from "./Title"
import {FaCocktail, FaHiking, FaShuttleVan, FaBeer} from "react-icons/fa"

export default class Services extends React.Component {
    state = {
        services: [
            {
                icon: <FaCocktail />,
                title: "free cocktails",
                info: "You are welcome to grab free cocktails in the happy hour. There is a welcome drink too on your arrival to quench away your thirst and refresh you. Nothing refreshes you like a free drink! Its on us for choosing us ;)"
            },
            {
                icon: <FaHiking />,
                title: "Endless Hiking",
                info: "Explore the scenic surroundings. You can wander off to nature on your own or can book a tour with us. It never hurts to leave your hectic life behind and enjoy in the lap of nature. Don't miss it! :P"
            },
            {
                icon: <FaShuttleVan />,
                title: "transportation",
                info: "With us leave all your worries about transporation behind. We provide free transfers from airport to our hotel. You just have to appear on time to catch the bus. Yes. we spoil you so bad that you don't want to leave :D."
            },
            {
                icon: <FaBeer />,
                title: "Bottomless Beers",
                info: "Yes, that's the highlight of our hotel. We serve beer from every corner of the world. You name it aaannnd we've got them! Never ending range of beer is available for you and who does not want a beer after a really tiring hike. :O"
            }
        ]
    }

    render() {
        return (
            <section className="services">
                <Title title="services" />
                <div className="services-center">
                    {this.state.services.map((item, index) => {
                        return (
                            <article key={index} className="service">
                                <span>{item.icon}</span>
                                <h6>{item.title}</h6>
                                <p>{item.info}</p>
                            </article>
                        )
                    }

                    )}

                </div>
            </section>
        )
    }
}